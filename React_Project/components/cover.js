import React from "react";
import { View, Image, StyleSheet } from 'react-native';


export default function Cover({images, small}){

    const {smallStyle, container } = styles
    const getStylesBook = () => {
        return small ? smallStyle : container
    }

  return(
    <View>
        <Image style={getStylesBook()} source={images} />
        {/* <Image source={{uri: `${book}`}} /> */}
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 60,
        borderColor: 'white',
        width: 230,
        height: 350,
        borderRadius: 15
    },
    smallStyle: {
        width: 100,
        height: 209,
        borderRadius: 10,
        margin: 3
    }
})