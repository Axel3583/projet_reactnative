import React from "react";
import { FlatList, TouchableOpacity } from "react-native";
import { StyleSheet } from 'react-native';
import Cover from "./cover";

export default function CoverList({ images }) {

    const renderListCover = (item) => {

        return (
            <TouchableOpacity>
                <Cover small={true} images={item.imageSrc} />
            </TouchableOpacity>
        )
    }

    return (
        <FlatList style={styles.container}
            data={images}
            horizontal={true}
            renderItem={({ item }) => renderListCover(item)}
            keyExtractor={item => item.id}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        width: 380,
        marginTop: 30,
        paddingHorizontal: 10
    }
})