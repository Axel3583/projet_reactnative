import React from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet, View, Text } from "react-native";


export default function Action() {

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => { console.log('ok') }} style={styles.price}>
                <Text style={styles.priceStyle}>19.99$</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { console.log('ok2') }} style={styles.free}>
                <Text style={styles.textStyle}>Free Preview</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        width: 300
    },
    price: {
        color: 'white',
        width: 100,
        backgroundColor: '#fffff0',
        justifyContent: 'center',
        textAlign: 'center',
        height: 50,
        borderBottomLeftRadius: 15
    },
    free: {
        color: 'white',
        width: 100,
        fontWeight: 500,
        backgroundColor: '#ff8c00',
        textAlign: 'center',
        justifyContent: 'center',
        height: 50,
        borderTopRightRadius: 15
    },

    textStyle: {
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold'
    },
    priceStyle: {
        fontWeight: 'bold',
        fontFamily: 'Montserrat-SemiBold'
    }
})