import React from "react";
import { StyleSheet, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import { prefix } from '../utils/prefix'


export default function Header(props) {

    return (
        <View style={styles.container2}>
            <Ionicons style={styles.icon1} name={`${prefix}-close`} size={32} />
            <Ionicons style={styles.icon2} name={`${prefix}-cart`} size={32} />
        </View>
    )
}

const styles = StyleSheet.create({
    container2: {
        flexDirection: 'row',
        marginTop: 20

    },
    icon1: {
        width: 100,
        color: 'white'
    },
    icon2: {
        width: 100,
        color: 'white',
        textAlign: 'right'
    },
    img: {
        color: 'red',
        marginTop: 50
    }
})