import React from "react";
import { StyleSheet, View, Text } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import { prefix } from '../utils/prefix'

export default function Ranking() {

    return (
        <View style={styles.container}>
            <Ionicons style={styles.start} name={`${prefix}-star`} size={32} />
            <Text style={styles.text}>4.8  (2578)</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,
        paddingHorizontal: 35,
        width: 170,
        maxHeight: 20

    },
    start: {
        color: '#ffd700',
        fontSize: 10,
        marginTop: 3
    },
    text: {
        color: 'white',
        fontFamily: 'Montserrat-medium',
    }
})