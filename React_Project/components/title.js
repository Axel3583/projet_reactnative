import React from "react";
import { View, Text, StyleSheet } from 'react-native';


export default function Title(){

    return(
        <View>
            <Text style={styles.tile}>The Jungle Book</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    tile: {
        color: 'white',
        fontFamily: 'GT-Sectra-Fine-Regular',
        marginTop: 20,
        fontSize: 30,
        fontWeight: 400
    }
})