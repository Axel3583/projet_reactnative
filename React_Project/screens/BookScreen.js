import { ScrollView, StyleSheet } from 'react-native'
import { View, Text } from "react-native-web";
import Cover from '../components/cover';
import Header from '../components/headerScreen';
import Ranking from '../components/ranking';
import Title from '../components/title';
import Action from '../components/action';
import CoverList from '../components/coverList';


export default function BookScreen() {

    const images = [
        { imageSrc: require('../assets/images/book1.png'), id: '1' },
        { imageSrc: require('../assets/images/book2.png'), id: '2' },
        { imageSrc: require('../assets/images/book3.png'), id: '3' },
        { imageSrc: require('../assets/images/book4.png'), id: '4' },
        { imageSrc: require('../assets/images/book5.png'), id: '5' },
        { imageSrc: require('../assets/images/book6.png'), id: '6' },
        { imageSrc: require('../assets/images/book1.png'), id: '7' },
        { imageSrc: require('../assets/images/book3.png'), id: '8' },
        { imageSrc: require('../assets/images/book2.png'), id: '9' },
        { imageSrc: require('../assets/images/book4.png'), id: '10' },

    ]

    const book = require("../assets/images/book1.png")

    return (
       
            <View style={styles.container}>
                <Header />
                <Cover images={book} />
                <Title />
                <Text style={styles.sbTitle}>Rudyard Kliping</Text>
                <Ranking />
                <Action />
                <CoverList images={images} />
            </View>
      
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        flexDirection: 'column',
        alignItems: 'center'
    },
    sbTitle: {
        color: 'white',
        fontFamily: 'Montserrat-Medium',
        marginTop: 10,
        fontSize: 20,
        fontWeight: 400,
        opacity: 0.5
    }
})