import React, { useState, useEffect } from "react"
import BookScreen from "./screens/BookScreen";
import * as Font from 'expo-font';
import { StyleSheet, View, Text, Platform } from "react-native";


export default function App() {

  useEffect(() => {
    loadFont(),
    console.log('platForm :', Platform.OS);
  }, [])
  const [loading, setLoading] = useState(true);
  const loadFont = async () => {
    try {
      await Font.loadAsync({
        'Gilroy-Bold': require("./fonts/Gilroy-Bold.ttf"),
        'GT-Sectra-Fine-Regular': require("./fonts/GT-Sectra-Fine-Regular.ttf"),
        'Montserrat-Black': require("./fonts/Montserrat-Black.ttf"),
        'Montserrat-Medium': require("./fonts/Montserrat-Medium.ttf"),
        'Montserrat-SemiBold': require("./fonts/Montserrat-SemiBold.ttf")
      })
      setLoading(false)
    } catch (error) {
      console.error('error :', error);
    }
  }

  if (loading) {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    )
  }

  return (
    <BookScreen />
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

